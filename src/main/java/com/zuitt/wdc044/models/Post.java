package com.zuitt.wdc044.models;

import javax.persistence.*;

// mark this Java object as a representation of a database table via @Entity
@Entity
// designate the table name
@Table(name = "posts")
public class Post {
    // indicates that this property is the primary key.
    @Id
    // auto-increment
    @GeneratedValue
    private Long id;

    // class properties that represent a table column
    @Column
    private String title;

    @Column
    private String content;

    @ManyToOne
    // to reference to the foreign key of the other entity
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    //default constructor, this is needed when retrieving posts
    public Post(){}

    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    //getter & setter
    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
