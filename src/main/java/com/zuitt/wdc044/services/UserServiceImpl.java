package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

// This is used to indicate that it holds the actual business logic.
@Service
public class UserServiceImpl implements UserService {
    // This is used to access objects and methods of the another class.
    @Autowired
    private UserRepository userRepository;

    // create user
    public void createUser(User user) {
        userRepository.save(user);
    }

    // Check if user exists
        // returns the entity based on the given criteria
        // or an empty instance of the optional class
    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable(userRepository.findByUsername(username));
    }

}
