package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {
    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    // creating a post
    public void createPost(String stringToken, Post post) {
        // findByUsername to retrieve the user
        // Criteria for finding the user is from the jwtToken method (getUsernameFromToken)
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Post newPost = new Post();
        // Title and content will come from the reqBody which is passed through the "post" identifier.
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());

        // author retrieved from the token
        newPost.setUser(author);

        // actual saving of post
        postRepository.save(newPost);
    }

    // getting all posts
    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }

    // edit a post
    public ResponseEntity updatePost(Long id, String stringToken, Post post) {
        // We will retrieve the post for updating using the id provided.
        Post postForUpdating = postRepository.findById(id).get();

        // Because relationship is established within the User and Post model, we are able to retrieve the username of the post owner.
        String postAuthor = postForUpdating.getUser().getUsername();

        // We will retrieve the username of the currently logged-in user in the token.
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        // This will check if the currently logged-in user is the owner of the post
        if (authenticatedUser.equals(postAuthor)) {
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);

            return new ResponseEntity("Post updated successfully", HttpStatus.OK);
        }
        else {
            return new ResponseEntity("You are not authorized to edit this Post.", HttpStatus.UNAUTHORIZED);
        }
    }
    
    
    // deleting a post
    public ResponseEntity deletePost(Long id, String stringToken) {
        Post postForDeleting = postRepository.findById(id).get();
        String postAuthor = postForDeleting.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if (authenticatedUser.equals(postAuthor)) {
            postRepository.deleteById(id);
            return new ResponseEntity("You have deleted your post.", HttpStatus.OK);
        }
        else {
            return new ResponseEntity("You are not authorized to delete!", HttpStatus.UNAUTHORIZED);
        }
    }

    // getting a user's post
    /*public Iterable<Post> myPosts(Long id, String stringToken) {
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));


    }*/

}







