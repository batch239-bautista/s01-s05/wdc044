package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    void createPost(String stringToken, Post post);

    // getting all posts
    Iterable<Post> getPosts();

    // Edit a user
    ResponseEntity updatePost(Long id, String stringToken, Post post);

    // Delete a user post
    ResponseEntity deletePost(Long id, String stringToken);

    // getting a user's post
//    Iterable<Post> myPosts(Long id, String stringToken);

}